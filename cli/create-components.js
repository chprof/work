const fs = require('fs');
const components = require('./components');

const createNavList = (component) => {
  return `<li class="nav-item"><a class="nav-link text-white" href="#${component.name}">${component.name}</a></li>\n`;
};

const createContentList = (component) => {
  return `<div class="section section--fullpage" id="${component.name}">
												<div class="section__in">
													<div class="container-fluid">
														<h2 class="section__title-guide">${component.name}</h2>
														<hr style="margin-bottom: 3em;" />
														<div>@@include('./src/partials/components/${component.name}.html')</div>
													</div>
												</div>
											</div>`;
};

const createComponentsScssListToImport = (component) => {
	if (component.template.scss) {
  	return `@import './components/${component.name}';\n`;
	}
	return '';
};

const createComponentsJsListToImport = (component) => {
  if (component.template.js) {
    return `@@include('./components/${component.name}.js')\n`;
  }
  return '';
};

const createGuideFiles = () => {
  const newComponents = components.filter((component) => !component.ignore);
  const guideHTML = fs.readFileSync('./views/guide.html', 'utf-8');
  let totalNavStr = '';
  let totalContentStr = '';
  let scssComponentsStr = '';
  let jsComponentsStr = '';
  newComponents.forEach((component) => {
    totalNavStr += createNavList(component);
    totalContentStr += createContentList(component);
    scssComponentsStr += createComponentsScssListToImport(component);
    jsComponentsStr += createComponentsJsListToImport(component);
    if (
      component.template.html &&
      !fs.existsSync(`../src/partials/components/${component.name}.html`)
    ) {
      console.log('has template and not in project');
      fs.copyFile(
        component.template.html,
        `../src/partials/components/${component.name}.html`,
        () => {}
      );
    }
    if (
      component.template.scss &&
      !fs.existsSync(`../src/styles/components/_${component.name}.scss`)
    ) {
      fs.copyFile(
        component.template.scss,
        `../src/styles/components/_${component.name}.scss`,
        () => {}
      );
    }
    if (
      component.template.js &&
      !fs.existsSync(`../src/scripts/components/${component.name}.js`)
    ) {
      fs.copyFile(
        component.template.js,
        `../src/scripts/components/${component.name}.js`,
        () => {}
      );
    }
  });

  let newGuideHTML = guideHTML
    .replace(/@@nav/g, totalNavStr)
    .replace(/@@content/g, totalContentStr);
  fs.writeFile('../src/views/guide.html', newGuideHTML, () => {});
  fs.writeFile('../src/styles/_components.scss', scssComponentsStr, () => {});
  fs.writeFile('../src/scripts/components.js', jsComponentsStr, () => {});
};

createGuideFiles();
