module.exports = [
	{
		name: 'svg-icon',
		ignore: false,
		template: {
			html: './components/svg-icon/svg-icon.html',
			scss: './components/svg-icon/_svg-icon.scss',
		},
	},
	{
		name: 'badge',
		ignore: false,
		template: {
			html: './components/badge/badge.html',
			scss: './components/badge/_badge.scss'
		},
	},
	{
		name: 'tag',
		ignore: false,
		template: {
			html: './components/tag/tag.html',
			scss: './components/tag/_tag.scss'
		},
	},
	{
		name: 'typography',
		ignore: false,
		template: {
			html: './components/typography/typography.html',
		},
	},
	{
		name: 'title',
		ignore: false,
		template: {
			scss: './components/title/_title.scss',
			html: './components/title/title.html',
		},
	},
	{
		name: 'text',
		ignore: false,
		template: {
			scss: './components/text/_text.scss',
			html: './components/text/text.html',
		},
	},
	{
		name: 'icon-text',
		ignore: false,
		template: {
			scss: './components/icon-text/_icon-text.scss',
			html: './components/icon-text/icon-text.html',
		},
	},
	{
		name: 'button',
		ignore: false,
		template: {
			html: './components/button/button.html',
			scss: './components/button/_button.scss'
		},
	},
	{
		name: 'button-close',
		ignore: false,
		template: {
			html: './components/button-close/button-close.html',
			scss: './components/button-close/_button-close.scss'
		},
	},
	{
		name: 'link',
		ignore: false,
		template: {
			html: './components/link/link.html',
			scss: './components/link/_link.scss'
		},
	},
	{
		name: 'logo',
		ignore: false,
		template: {
			html: './components/logo/logo.html',
			scss: './components/logo/_logo.scss'
		},
	},
	{
		name: 'sticky',
		ignore: false,
		template: {
			html: './components/sticky/sticky.html',
			scss: './components/sticky/_sticky.scss'
		},
	},
	{
		name: 'dropdown',
		ignore: false,
		template: {
			html: './components/dropdown/dropdown.html',
			scss: './components/dropdown/_dropdown.scss'
		},
	},
	{
		name: 'list',
		ignore: false,
		template: {
			html: './components/list/list.html',
			scss: './components/list/_list.scss'
		},
	},
	{
		name: 'form',
		ignore: false,
		template: {
			html: './components/form/form.html',
			scss: './components/form/_form.scss'
		},
	},
	{
		name: 'carousel',
		ignore: false,
		template: {
			html: './components/carousel/carousel.html',
			scss: './components/carousel/_carousel.scss',
			js: './components/carousel/carousel.js'
		},
	},
	{
		name: 'modal',
		ignore: false,
		template: {
			html: './components/modal/modal.html',
			scss: './components/modal/_modal.scss'
		},
	},
	{
		name: 'card',
		ignore: false,
		template: {
			html: './components/card/card.html',
			scss: './components/card/_card.scss'
		},
	},
	{
		name: 'pagination',
		ignore: false,
		template: {
			html: './components/pagination/pagination.html',
			scss: './components/pagination/_pagination.scss'
		},
	},
	{
		name: 'tooltip',
		ignore: true,
		template: {
			html: './components/tooltip/tooltip.html',
			scss: './components/tooltip/_tooltip.scss'
		},
	},
	{
		name: 'breadcrumbs',
		ignore: false,
		template: {
			html: './components/breadcrumbs/breadcrumbs.html',
			scss: './components/breadcrumbs/_breadcrumbs.scss'
		},
	},
	{
		name: 'accordion',
		ignore: false,
		template: {
			html: './components/accordion/accordion.html',
			scss: './components/accordion/_accordion.scss'
		},
	},
	{
		name: 'collapse',
		ignore: false,
		template: {
			html: './components/collapse/collapse.html',
			scss: './components/collapse/_collapse.scss'
		},
	},
	{
		name: 'table',
		ignore: false,
		template: {
			html: './components/table/table.html',
			scss: './components/table/_table.scss'
		},
	},
	{
		name: 'tabs',
		ignore: false,
		template: {
			html: './components/tabs/tabs.html',
			scss: './components/tabs/_tabs.scss'
		},
	},
	{
		name: 'editor',
		ignore: false,
		template: {
			html: './components/editor/editor.html',
			scss: './components/editor/_editor.scss',
		},
	},
	{
		name: 'wrap',
		ignore: false,
		template: {
			html: './components/wrap/wrap.html',
			scss: './components/wrap/_wrap.scss',
		},
	},
	
]