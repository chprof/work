const fs = require('fs');
const vars = require('./vars');

const scssFileConfig = {
	name: '../src/styles/_config.scss',
	path: ''
};


const createSCSSConfigStr = (group, str) => {
	str += `//${group.name}\n//============\n\n`;
	if(group.name != 'optionsConfig' ) {
		group.options.forEach(option => {
			const optionArr = Object.entries(option);
			const key = optionArr[0][0];
			const value = optionArr[0][1];
			str += `${key}: ${value};\n`;
		})
	} else {
		const config = group.options;
		const convertedStr = '$config: ' + JSON.stringify(config).replace(/\[/g, '(').replace(/\]/g, ')').replace(/\{/g, '(').replace(/\}/g, ')');
		str += convertedStr + '\n';
	}
	str += '\n//============\n//============\n\n';
	return str;
};


const createSassConfig = () => {
	let str = '';
	vars.forEach(group => {
		str = createSCSSConfigStr(group, str);
	})
	fs.writeFile(scssFileConfig.name, str, (err) => {
		if (err) throw err;
		console.log('scss file writed');
	});
};


createSassConfig();