const fontSizeRelative = 17,
	$xs = 0,
	$sm = 576,
	$md = 768,
	$lg = 992,
	$xl = 1200,
	$xxl = 1400;

	
module.exports = [
	{
		name: 'media',
		options: [
			{ '$xs': 0 },
			{ '$sm': 576 },
			{ '$md': 768 },
			{ '$lg': 992 },
			{ '$xl': 1200 },
			{ '$xxl': 1400 },
		]
	},
	{
		name: 'global',
		options: [
			{'$font': '\'IBM Plex Sans\''},
			{'$color': '#383B49'},
			{'$linkColor': '#7D7AFF'},
			{'$backgroundColor': '#F5F5F5'},
			{'$borderColor': '#F5F5F5'},
			{'$fontSizeRelative': fontSizeRelative},
			{'$fontSize': '1rem'},
			{'$isBootstrap': true},
		]
	},
	{
		name: 'optionsConfig',
		options: [
			{
				name: 'grid',
				selector: "container",
				options: [
					{
						selector: '',
						value: '1320px',
						gutters: ''
					},
				]
			},
			{
				name: 'font-family',
				selector: "ff",
				options: [
					{
						name: 'IBM Plex Sans',
						src: '../fonts/ibm-plex-sans/ibm-plex-sans',
						options: [
							{
								weight: 100,
								style: 'normal'
							},
							{
								weight: 100,
								style: 'italic'
							},
							{
								weight: 200,
								style: 'normal'
							},
							{
								weight: 200,
								style: 'italic'
							},
							{
								weight: 300,
								style: 'normal'
							},
							{
								weight: 300,
								style: 'italic'
							},
							{
								weight: 400,
								style: 'normal'
							},
							{
								weight: 400,
								style: 'italic'
							},
							{
								weight: 500,
								style: 'normal'
							},
							{
								weight: 500,
								style: 'italic'
							},
							{
								weight: 600,
								style: 'normal'
							},
							{
								weight: 600,
								style: 'italic'
							},
							{
								weight: 700,
								style: 'normal'
							},
							{
								weight: 700,
								style: 'italic'
							},
						]
					}
				]
			},
			{
				name: 'font-size-root',
				selector: "fzr",
				options: [
					{
						mediaString: 'xs',
						media: $xs,
						value: '13px'
					},
					{
						mediaString: 'md',
						media: $md,
						value: '15px'
					},
					{
						mediaString: 'xxl',
						media: $xxl,
						value: '17px'
					},
				]
			},
			{
				name: 'font-size-title',
				selector: "h",
				options: [
					{ '1': 36 / fontSizeRelative + 'rem' },
					{ '2': 25 / fontSizeRelative + 'rem' },
					{ '3': 18 / fontSizeRelative + 'rem' },
					{ '4': 17 / fontSizeRelative + 'rem' },
					{ '5': 15 / fontSizeRelative + 'rem' },
					{ '6': 12 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'font-size-text',
				selector: "text",
				options: [
					{ '1': 17 / fontSizeRelative + 'rem' },
					{ '2': 16 / fontSizeRelative + 'rem' },
					{ '3': 15 / fontSizeRelative + 'rem' },
					{ '4': 14 / fontSizeRelative + 'rem' },
					{ '5': 12 / fontSizeRelative + 'rem' },
					{ '6': 8 / fontSizeRelative + 'rem' },
				]
			},
			{
				name: 'border-radius',
				selector: "bdrs",
				options: [
					{ '0': 5 / fontSizeRelative + 'rem' },
					{ '1': 3 / fontSizeRelative + 'rem' },
					{ '2': 10 / fontSizeRelative + 'rem' },
					{ '3': 50 / fontSizeRelative + 'rem' },
				]
			},
			{
				name: 'colors',
				selector: "cf",
				options: [
					{
						name: 'dark',
						selector: "1",
						options: [
							{ '0': '#383B49' },
							{ '1': '#000' },
							{ '2': '#364051' },
							{ '3': 'rgba(40, 47, 54, 0.8)' },
						]
					},
					{
						name: 'gray',
						selector: "2",
						options: [
							{ '0': '#686E7D' },
							{ '1': '#969FA9' },
							{ '2': '#E9E9E9' },
							{ '3': 'rgba(40, 47, 54, 0.3)' },
							{ '4': 'rgba(40, 47, 54, 0.8)' },
							{ '5': '#DADADA' },
							{ '6': '#C4C4C4' },
						]
					},
					{
						name: 'blue',
						selector: "3",
						options: [
							{ '0': '#7D7AFF' },
							{ '1': '#1D3B95' },
							{ '2': '#0097C6' },
							{ '3': '#3CBEE7' },
							{ '4': '#3C62E7' },
						]
					},
					{
						name: 'white',
						selector: "4",
						options: [
							{ '0': '#fff' },
						]
					},
					{
						name: 'red',
						selector: "5",
						options: [
							{ '0': '#D76060' },
							{ '1': '#FF4949' },
							{ '2': '#E73C3C' },
						]
					},
					{
						name: 'green',
						selector: "6",
						options: [
							{ '0': '#71A862' },
						]
					},
				]
			},
			{
				name: 'background-color',
				selector: "bgc",
				options: [
					{
						name: 'gray',
						selector: "0",
						options: [
							{'0': '#F5F5F5'},
							{'1': '#FDFDFD'},
							{'2': '#969FA9'},
							{'3': '#E9E9E9'},
							{'4': 'rgba(233, 233, 233, 0.3)'},
							{'5': 'rgba(104, 110, 125, 0.1)'},
						]
					},
					{
						name: 'white',
						selector: "1",
						options: [
							{'0': '#fff'},
							{'1': 'rgba(255, 255, 255, 0.1)'},
						]
					},
					{
						name: 'blue',
						selector: "2",
						options: [
							{'0': '#94DBF1'},
							{'1': 'rgba(60, 190, 231, 0.2)'},
							{'2': 'rgba(125, 122, 255, 0.3)'},
							{'3': '#7D7AFF'},
							{'4': 'rgba(60, 98, 231, 0.2)'},
							{'5': '#1D3B95'},
							{'6': 'rgba(125, 122, 255, 0.1)'},
							{'7': 'rgba(29, 59, 149, 0.05)'},
						]
					},
					{
						name: 'red',
						selector: "3",
						options: [
							{'0': '#F5B1B1'},
							{'1': 'rgba(215, 96, 96, 0.1)'},
							{'2': 'rgba(231, 60, 60, 0.2)'},
							{'3': '#D76060'},
						]
					},
					{
						name: 'dark',
						selector: "4",
						options: [
							{'0': '#383B49'},
							{'1': 'rgba(0, 0, 0, 0.3)'},
						]
					},
					{
						name: 'green',
						selector: "5",
						options: [
							{'0': '#71A862'},
							{'1': 'rgba(113, 168, 98, 0.2)'},
						]
					},
				]
			},
			{
				name: 'border-color',
				selector: "bdc",
				options: [
					{
						name: 'gray',
						selector: "0",
						options: [
							{'0': '#F5F5F5'},
							{'1': 'rgba(40, 47, 54, 0.15)'},
							{'2': '#E4E4E4'},
							{'3': '#E9E9E9'},
							{'4': '#F5F6FA'},
							{'5': '#969FA9'},
							{'6': 'rgba(150, 159, 169, 0.3)'},
						]
					},
					{
						name: 'white',
						selector: "1",
						options: [
							{'0': 'rgba(255, 255, 255, 0.2)'},
							{'1': 'rgba(255, 255, 255, 1)'},
						]
					},
					{
						name: 'blue',
						selector: "2",
						options: [
							{'0': 'rgba(29, 59, 149, 0.3)'},
							{'1': '#7D7AFF'},
							{'2': '#1D3B95'},
						]
					},
					{
						name: 'red',
						selector: "3",
						options: [
							{'0': '#D76060'},
						]
					},
					{
						name: 'dark',
						selector: "4",
						options: [
							{'0': '#686E7D'},
						]
					},
					{
						name: 'green',
						selector: "5",
						options: [
							{ '0': '#71A862' },
						]
					},
				]
			},
			{
				name: 'box-shadow-color',
				selector: "bxsh",
				options: [
					{
						name: 'dark',
						selector: "1",
						options: [
							{'0': 'rgba(61, 58, 194, 0.12)'},
							{'1': 'rgba(61, 58, 194, 0.08)'},
							{'2': 'rgba(0, 0, 0, 0.12)'},
							{'3': 'rgba(0, 0, 0, 0.1)'},
							{'4': 'rgba(0, 0, 0, 0.7)'},
						]
					},
				]
			},
		]
	}
];