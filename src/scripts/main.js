document.addEventListener("DOMContentLoaded", () => {
  svg4everybody();
	var animateCSS = function(element, animation, time, prefix = 'animate__') {
		return new Promise((resolve, reject) => {
			var animationName = `${prefix}${animation}`;
			var node = document.querySelector(element);
			
			node.classList.add(`${prefix}animated`, animationName);

			function handleAnimationEnd(event) {
				event.stopPropagation();
				setTimeout(function() {
					node.classList.remove(`${prefix}animated`, animationName);
					resolve('Animation ended');
				}, time)
			}

			node.addEventListener('animationend', handleAnimationEnd, {once: true});
		});
	};
  (function() {
    var forms = document.querySelectorAll('.needs-validation');  
    validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if(form.dataset.element === 'step-form') {
					event.preventDefault();
					event.stopPropagation();
					if (form.checkValidity() === false) {
						form.classList.add('has-errors');
					} else {
						form.classList.remove('has-errors');
					}
				} else if(form.dataset.submit === 'true') {
					event.preventDefault();
					event.stopPropagation();

					// ajax logic here
				} else {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
				}
				form.classList.add('was-validated');
			}, false);
    });
  })();


	(function() {
		$('[data-element="select"]').each(function(i, el) {
			$(el).select2({});
			if ($(el).attr('multiple') && $(el).data('search') === false) {
				$(el).on('select2:opening select2:closing', function( event ) {
						var $searchfield = $(this).parent().find('.select2-search__field');
						$searchfield.prop('disabled', true);
				});
			}
		})
	})();
	(function() {
		var togglePasswords = $('[data-element="password-toggle"]');
		togglePasswords.each(function(i, toggle) {
			var input = $(toggle).parent().find('input');
			$(toggle).on('click', function(e) {
				if(input.attr('type') == 'text') {
					input.attr('type', 'password');
				} else if(input.attr('type') == 'password') {
					input.attr('type', 'text');
				}
			})
		})
	})();
	(function() {
		var formSearchs = document.querySelectorAll('[data-element="form-search"]');
		Array.prototype.filter.call(formSearchs, function(form) {
			var anchors = form.querySelectorAll('[data-element="anchor"]');
			var input = form.querySelector('[data-element="input"]');
			var clear = form.querySelector('[data-element="clear"]');

			Array.prototype.filter.call(anchors, function(anchor) {
				// 
				anchor.addEventListener('click', function(e) {
					e.preventDefault();
					input.value = e.currentTarget.textContent;
					clear.classList.add('visible');
				})
			});
			clear.addEventListener('click', function() {
				input.value = '';
				clear.classList.remove('visible');
			})
		})
	})();
	(function() {
		var formCounters = document.querySelectorAll('[data-element="form-counter"]');
		Array.prototype.filter.call(formCounters, function(counter) {
			var plus = counter.querySelector('[data-element="button-counter-plus"]');
			var minus = counter.querySelector('[data-element="button-counter-minus"]');
			var input = counter.querySelector('[data-element="form-counter-input"]');

			plus.addEventListener('click', function(e) {
				if(!input.value) {
					input.value = 1
				}
				input.value++
			})
			minus.addEventListener('click', function(e) {
				if(input.value > 1) {
					input.value--
				} else {
					input.value = 1
				}
			})
		})
	})();
	(function() {
		var formDatepickers = document.querySelectorAll('[data-element="datepicker"]');
		Array.prototype.filter.call(formDatepickers, function(formDatepicker) {
			var id = formDatepicker.parentElement.id;
			$(formDatepicker).datepicker({
				container: `#${id}`,
				language: 'ru'
			});
			formDatepicker.nextElementSibling.addEventListener('click', function(e) {
				e.currentTarget.classList.toggle('active');
				if(e.currentTarget.classList.contains('active')) {
					$(formDatepicker).datepicker('show')
				} else {
					$(formDatepicker).datepicker('hide')
				}
			})
		})
	})();
	
	(function() {
		var toggles = document.querySelectorAll('[data-element="toggle"]');
		Array.prototype.filter.call(toggles, function(toggle) {
			toggle.addEventListener('click', function(e) {
				var _targetSelector = e.currentTarget.dataset.target;
				document.querySelector(_targetSelector).classList.toggle('visible');
			})
		})
	})();
	
	(function() {
		var editors = document.querySelectorAll('[data-element="editor"]');
		Array.prototype.filter.call(editors, function(editor) {
			ClassicEditor
        .create( editor )
        .catch( error => {
            console.error( error );
        } );
		})
	})();
	
	(function() {
		function readURL(e) {
			var _this = e.currentTarget;
			var _target = _this.dataset.target;
			if (_this.files && _this.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					document.querySelector(`#${_target}`).setAttribute('src', e.target.result)
				};

				reader.readAsDataURL(_this.files[0]);
			}
		};

		var formFiles = document.querySelectorAll('[data-element="form-file"]');
		var buttonsChangeFormFile = document.querySelectorAll('[data-element="form-file-change"]');

		Array.prototype.filter.call(formFiles, function(file) {
			file.addEventListener('change', function(e) {
				readURL(e)
			})
		})
		Array.prototype.filter.call(buttonsChangeFormFile, function(button) {
			button.addEventListener('click', function(e) {
				var _this = e.currentTarget;
				var _target = _this.dataset.target;
				document.querySelector(`#${_target}`).click();
			})
		})
	})();
	
	
	(function() {
		var tabsSteps = document.querySelectorAll('[data-element="tabs-steps"]');

		function setVisibilityRulesForToggles(toggles, _length, next, prev, finish) {
			Array.prototype.filter.call(toggles, function(toggle, i) {
				if(toggle.classList.contains('active')) {
					if(i !== 0 && i !== _length - 1) {
						next.parentElement.classList.remove('d-none');
						prev.parentElement.classList.remove('d-none');
						finish.parentElement.classList.add('d-none');
					} else if(i === 0) {
						next.parentElement.classList.remove('d-none');
						prev.parentElement.classList.add('d-none');
						finish.parentElement.classList.add('d-none');
					} else if(i === _length - 1) {
						prev.parentElement.classList.remove('d-none');
						next.parentElement.classList.add('d-none');
						finish.parentElement.classList.remove('d-none');
					}
				}
			})
		};

		
		Array.prototype.filter.call(tabsSteps, function(tabs) {
			var toggles = tabs.querySelectorAll('[data-bs-toggle="tab"]');
			var next = tabs.querySelector('[data-element="next"]');
			var prev = tabs.querySelector('[data-element="prev"]');
			var finish = tabs.querySelector('[data-element="finish"]');
			var stepSubmits = tabs.querySelectorAll('[data-element="step-submit"]');
			var _length = toggles.length;

			Array.prototype.filter.call(toggles, function(toggle, i) {
				if(toggle.classList.contains('active')) {
					if(i === 0) {
						prev.parentElement.classList.add('d-none');
					} else if(i === (_length - 1)) {
						finish.parentElement.classList.remove('d-none');
						next.parentElement.classList.add('d-none');
					}
				}
			});
			
			next.addEventListener('click', function(e) {
				e.preventDefault();
				var currentTab = tabs.querySelector('[data-bs-toggle="tab"].active');
				var nextTab = currentTab.parentElement.nextElementSibling.querySelector('[data-bs-toggle="tab"]');
				var currentForm = document.querySelector(currentTab.dataset.bsTarget).querySelector('form');
				var currentStepSubmit = document.querySelector(currentTab.dataset.bsTarget).querySelector('[data-element="step-submit"]');
				currentStepSubmit.click();
				if(!currentForm.classList.contains('has-errors') && currentTab.parentElement.nextElementSibling) {
					nextTab.click();
					setVisibilityRulesForToggles(toggles, _length, next, prev, finish);
				}
			})
			
			prev.addEventListener('click', function(e) {
				e.preventDefault();
				var currentTab = tabs.querySelector('[data-bs-toggle="tab"].active');
				if(currentTab.parentElement.previousElementSibling) {
					var prevTab = currentTab.parentElement.previousElementSibling.querySelector('[data-bs-toggle="tab"]');
					prevTab.click();
					setVisibilityRulesForToggles(toggles, _length, next, prev, finish);
				}
			});
			
			finish.addEventListener('click', function(e) {
				e.preventDefault();
				var currentTab = tabs.querySelector('[data-bs-toggle="tab"].active');
				var currentStepSubmit = document.querySelector(currentTab.dataset.bsTarget).querySelector('[data-element="step-submit"]');
				currentStepSubmit.click();
			})
		});
	})();
	
	(function() {
		var visibilityLabels = document.querySelectorAll('[data-element="form-switch-link"]');
		Array.prototype.filter.call(visibilityLabels, function(label, i) {
			label.addEventListener('click', function(e) {
				e.preventDefault();
				e.currentTarget.parentElement.nextElementSibling.classList.remove('d-none');
				e.currentTarget.classList.add('d-none');
			})
		})
	})();
	
	(function() {
		var formInputAddButton = document.querySelectorAll('[data-element="form-input-add"]');
		Array.prototype.filter.call(formInputAddButton, function(button, i) {
			button.addEventListener('click', function(e) {
				var placeholder = e.currentTarget.dataset.placeholder;
				e.preventDefault();
				e.currentTarget.parentElement.previousElementSibling.insertAdjacentHTML('afterend',
					`<div class="col">
						<input
							class="form-control form-control--secondary"
							id="skills"
							type="text"
							placeholder="${placeholder ? placeholder : 'Навык'}"
						/>
					</div>`);
			})
		})
	})();
	
	
	(function() {
		var tabsDropdownEl = document.querySelector('#tabs-dropdown');
		// 
		var triggerEls = document.querySelectorAll('[data-bs-toggle="tab"]');
		Array.prototype.filter.call(triggerEls, function(tab, i) {
			tab.addEventListener('shown.bs.tab', function(event) {
				tabsDropdownEl.innerHTML = event.target.innerHTML
			})
		})
	})();

	(function() {
		var formCheckboxToggle = document.querySelectorAll('[data-element="form-checkbox-toggle"]');
		Array.prototype.filter.call(formCheckboxToggle, function(toggle, i) {
			toggle.addEventListener('change', function(e) {
				var _this = e.target;
				var _target = _this.dataset.target;
				var _toggleElement = document.querySelector(`${_target}`);
				
				if(_this.checked === true) {
					_toggleElement.classList.add('d-none')
				} else {
					_toggleElement.classList.remove('d-none')
				}

			})
		})
	})();
	
	(function() {
		var formButtonsAdd = document.querySelectorAll('[data-element="form-exp-button-add"]');
		var count = 0;
		function changeIds(elems) {
			Array.prototype.filter.call(elems, function(elem, i) {
				elem.removeAttribute('id');
				elem.removeAttribute('for');
				elem.value = '';
				if(elem.parentElement.classList.contains('select')) {
					elem.parentElement.removeChild(elem.nextElementSibling);
					$(elem).select2('destroy');
					elem.parentElement.setAttribute('id', elem.parentElement.id + '-' + count);
					elem.setAttribute('data-dropdown-parent', elem.dataset.dropdownParent + '-' + count);
				}
			})
		};
		Array.prototype.filter.call(formButtonsAdd, function(button, i) {
			button.addEventListener('click', function(e) {
				var _this = e.currentTarget;
				var _target = _this.dataset.src;
				var _baseElement = document.querySelector(`${_target}`);
				var _baseElementCopy = _baseElement.cloneNode(true);
				var _parent = document.querySelector(`${_this.dataset.parent}`);
				_baseElementCopy.removeAttribute('id');
				var clonedInputs = _baseElementCopy.querySelectorAll('input');
				var clonedSelects = _baseElementCopy.querySelectorAll('select');
				var clonedTextAreas = _baseElementCopy.querySelectorAll('textarea');
				var clonedLabels = _baseElementCopy.querySelectorAll('label');
				changeIds(clonedInputs);
				changeIds(clonedSelects);
				changeIds(clonedTextAreas);
				changeIds(clonedLabels);
				_parent.insertAdjacentElement('beforeend', _baseElementCopy);
				Array.prototype.filter.call(document.querySelectorAll('[data-element="select"]'), function(select, i) {
					$(select).select2();
				});
				count++;
			})
		})
	})();
	
	(function() {
		var carouselBase = new Swiper("[data-element='carousel-base']", {
			slidesPerView: 'auto',
			spaceBetween: 80,
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			breakpoints: {
				0: {
					slidesPerView: 1,
					spaceBetween: 15,
				},
				450: {
					slidesPerView: 'auto',
					spaceBetween: 40,
				},
				992: {
					slidesPerView: 'auto',
					spaceBetween: 80,
				},
			},
		});
		var carouselSecondary = new Swiper("[data-element='carousel-secondary']", {
			slidesPerView: 2,
			spaceBetween: 30,
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			breakpoints: {
				0: {
					slidesPerView: 1,
					spaceBetween: 30,
				},
				576: {
					slidesPerView: 2,
					spaceBetween: 30,
				},
			},
		});
	})();
	
	@@include('./components.js')
});